FROM python:3.8

COPY requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt && \
    rm /tmp/requirements.txt

COPY ./nbastats /usr/local/nbastats/nbastats

ENV PYTHONPATH "$PYTHONPATH:/usr/local/nbastats"

CMD ["uvicorn", "--host=0.0.0.0", "nbastats.api:app"]