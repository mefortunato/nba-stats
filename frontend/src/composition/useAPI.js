import axios from 'axios'

export default function useAPI() {

  const API = axios.create({
    baseURL: process.env.VUE_APP_API_BASE,
  })

  return {
    API
  }
}
