import {
  createRouter,
  createWebHistory
} from 'vue-router';
import About from '../views/About.vue';
import Contest from '../views/Contest.vue';
import Home from '../views/Home.vue';
import Player from '../views/Player.vue';
import PageNotFound from '../views/PageNotFound.vue';

const routes = [{
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component: About,
  },
  {
    path: '/contest',
    name: 'Contest',
    component: Contest,
  },
  {
    path: '/player',
    name: 'Player',
    component: Player,
  },
  {
    path: '/:catchAll(.*)',
    name: 'Page not found',
    component: PageNotFound,
  }

];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
