module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: theme => ({
        'basketball': "url('https://images.unsplash.com/photo-1613265020375-38bdb1315e98?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80')",
      }),
      colors: {
        darkgray: "#3c3c3c",
        navbar: "#808080",
        lightgray: "#dfdfdf",
        mediumgray: "#888888",
        "light-table-row": "#c0c0c0",
        "dark-table-row": "#767676",
        "table-row-hover": "#fca31b",
        accent: "#e75b00",
        "accent-hover": "#ff7300",
        "accent-row-hover": "#ff9a4e"
      },
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
