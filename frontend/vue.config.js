module.exports = {
  pages: {
    index: {
      entry: 'src/main.js',
      title: 'NBA Stats'
    }
  },
  devServer: {
    disableHostCheck: true
  }
}
