import json
import random
from fastapi import FastAPI, Query
from fastapi.middleware.cors import CORSMiddleware
from typing import Optional, List

from ..data import players
from ..data.download import get_gamelogs, get_season_avgs, get_draftables

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

headers = {
    "User-Agent": "Mozilla/5.0 (X11; CrOS x86_64 13597.94.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.186 Safari/537.36"
}

@app.get("/players")
def get_players():
    return players

@app.get("/player/random")
def get_random_player():
    name, _id = random.choice(list(players.items()))
    return {'name': name, '_id': _id}

@app.get("/stats/player/gamelogs")
async def get_player_gamelogs(
    player_id: Optional[str] = '', 
    player_name: Optional[str] = '',
    year: Optional[int] = 2021, 
    expanding: Optional[List[int]] = Query([1, 3, 5, 10, 25, 50])
):
    if not player_id and player_name:
        player_id = players.get(player_name)
    if not player_id:
        return {}
    df = await get_gamelogs(player_id, year)
    df = df.sort_values('G', ascending=False)
    expanding_df = df.expanding().mean()
    resp = {
        'logs': df.to_dict(orient='records'),
        'expanding': {
            n: expanding_df.iloc[n-1].to_dict()
            for n in filter(lambda x: x <= len(expanding_df), expanding)
        }
    }
    return resp

@app.get("/stats/player/seasons")
async def get_player_season_avgs(player: str):
    df = await get_season_avgs(player)
    return df.sort_values('Age', ascending=False).to_dict(orient='records')

@app.get('/dk/draftables')
async def get_dk_draftables():
    draftables = await get_draftables()
    draftables = draftables.drop_duplicates('playerId')
    # fppg = draftables['draftStatAttributes'].apply(lambda x: x[0]['value']).astype(float)
    # draftables = draftables[fppg >= 3.0]
    return json.loads(draftables.to_json(orient='records'))

@app.get("/dk/player")
async def get_dk_player(player: str):
    draftables = await get_draftables()
    player_draftables = draftables[draftables['displayName'].str.lower() == player.lower()]
    player_draftables = player_draftables[['displayName', 'position', 'salary']].astype(str)
    if len(player_draftables) == 0:
      return {}
    return json.loads(player_draftables.iloc[0].to_json())