PLAYER_SEASON_BASIC_STATS = {
    'G': int,
    'GS': int,
    'MP': float,
    'FG': float,
    'FGA': float,
    'FG%': float,
    '3P': float,
    '3PA': float,
    '3P%': float,
    '2P': float,
    '2PA': float,
    '2P%': float,
    'eFG%': float,
    'FT': float,
    'FTA': float,
    'FT%': float,
    'ORB': float,
    'DRB': float,
    'TRB': float,
    'AST': float,
    'STL': float,
    'BLK': float,
    'TOV': float,
    'PF': float,
    'PTS': float,
}


PLAYER_GAMELOG_BASIC_STATS = {
    'G': int,
    'FG': int,
    'FGA': int,
    'FG%': float,
    '3P': int,
    '3PA': int,
    '3P%': float,
    'FT': int,
    'FTA': int,
    'FT%': float,
    'ORB': int,
    'DRB': int,
    'TRB': int,
    'AST': int,
    'STL': int,
    'BLK': int,
    'TOV': int,
    'PF': int,
    'PTS': int,
    'GmSc': float,
    '+/-': int
}