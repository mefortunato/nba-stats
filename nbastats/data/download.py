import aiohttp
import asyncio
import json
import requests
import pandas as pd
import string
from bs4 import BeautifulSoup
from typing import Union

from . import columns


async def async_get(session, url):
    async with session.get(url) as resp:
        if resp.status != 200:
            resp.raise_for_status()
        return await resp.text()


async def async_get_json(session, url):
    async with session.get(url) as resp:
        if resp.status != 200:
            resp.raise_for_status()
        return await resp.json()


def dkfp(df):
  doubles = (
    (df['PTS'] >= 10).astype(int) + 
    (df['TRB'] >= 10).astype(int) + 
    (df['AST'] >= 10).astype(int) + 
    (df['BLK'] >= 10).astype(int) + 
    (df['STL'] >= 10).astype(int)
  )
  double_double = (doubles >= 2).fillna(0).astype(int)
  triple_double = (doubles >= 3).fillna(0).astype(int)
  df['DKFP'] = (
    df['PTS'] + 0.5*df['3P'] + 1.25*df['TRB'] + 
    1.5*df['AST'] + 2*df['STL'] + 2*df['BLK'] + 
    -0.5*df['TOV'] + 1.5*double_double + 3*triple_double
  )


async def get_players(path: str = 'data/players.json'):
    urls = [
        f'https://www.basketball-reference.com/players/{letter}/'
        for letter in string.ascii_lowercase
    ]
    tasks = []
    async with aiohttp.ClientSession() as session:
        for url in urls:
            task = asyncio.create_task(async_get(session, url))
            tasks.append(task)
        player_pages = await asyncio.gather(*tasks)
    players = {}
    for page in player_pages:
        soup = BeautifulSoup(page, 'lxml')
        for th in soup.find_all('th', {'data-stat': 'player'}):
            a = th.find('a')
            if a is None:
                continue
            player_name = a.text
            players[player_name] = a['href'].split('/')[-1][:-5]
    if path is not None:
        with open(path, 'w') as f:
            json.dump(players, f)
    return players


async def get_gamelogs(player: str, year: int) -> Union[pd.DataFrame, None]:
    url = f'https://www.basketball-reference.com/players/{player[0]}/{player}/gamelog/{year}'
    async with aiohttp.ClientSession() as session:
        text = await async_get(session, url)
        df =  pd.read_html(text, attrs={'id': 'pgl_basic'})
        if not df or len(df) != 1:
            return None
        df = df[0]
        df['G'] = df['G'].fillna(0)
        mask = df['Rk'].astype(str).str.isnumeric() == True
        df = df[mask].drop('G', axis=1)
        df = df.replace('Did Not Play', 0.0).replace('Inactive', 0.0).replace('Did Not Dress', 0.0)
        df = df.rename(columns={
            'Rk': 'G',
            'Unnamed: 5': 'H/A',
            'Unnamed: 7': 'W/L'
        })
        df[['W/L', 'Tm+/-']] = df['W/L'].str.split(expand=True)
        df['Tm+/-'] = df['Tm+/-'].str.slice(1, -1).astype(int)
        df = df.astype(columns.PLAYER_GAMELOG_BASIC_STATS).fillna(0.0)
        dkfp(df)
        return df


async def get_season_avgs(player: str) -> Union[pd.DataFrame, None]:
     url = f'https://www.basketball-reference.com/players/{player[0]}/{player}.html'
     async with aiohttp.ClientSession() as session:
        text = await async_get(session, url)
        df =  pd.read_html(text, attrs={'id': 'per_game'})
        if not df or len(df) != 1:
            return None
        df = df[0]
        mask = ~df['Age'].isna()
        df = df[mask]
        df = df.replace('Did Not Play', 0.0).replace('Inactive', 0.0)
        df = df.astype(columns.PLAYER_SEASON_BASIC_STATS)
        return df.fillna(0.0)


async def get_draftables() -> dict:
    contest_url = 'https://www.draftkings.com/lobby/getcontests?sport=NBA'
    async with aiohttp.ClientSession() as session:
        contests = await async_get_json(session, contest_url)
        draft_group = list(filter(lambda x: x['ContestTypeId'] == 70 and x['ContestStartTimeSuffix'] is None, contests['DraftGroups']))[0]['DraftGroupId']
        draftables_url = f'https://api.draftkings.com/draftgroups/v1/draftgroups/{draft_group}/draftables?format=json'
        draftables = await async_get_json(session, draftables_url)
        return pd.DataFrame(draftables.get('draftables'))


if __name__ == '__main__':
    df = asyncio.run(get_season_avgs('barklch01'))