import os
import json

current_dir = os.path.dirname(__file__)

with open(f'{current_dir}/players.json') as f:
    players = json.load(f)