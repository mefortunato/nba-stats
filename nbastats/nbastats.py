import os
import re
import json
import datetime
import requests
import pandas as pd

GAMES_DAY_URL = 'https://www.basketball-reference.com/boxscores/?month={month}&day={day}&year={year}'
BOX_SCORE_URL = 'https://www.basketball-reference.com/boxscores/{game_id}.html'

COLUMN_MAPPING = {
    'Starters': 'PLAYER', 
    'FG%': 'FG_PCT',
    '3P': '_3P',
    '3PA': '_3PA',
    '3P%': '_3P_PCT', 
    'FT%': 'FT_PCT', 
    '+/-': 'PLUS_MINUS',
    'TS%': 'TS_PCT',
    'eFG%': 'eFG_PCT',
    '3PAr': '_3PAr',
    'ORB%': 'ORB_PCT',
    'DRB%': 'DRB_PCT',
    'TRB%': 'TRB_PCT',
    'AST%': 'AST_PCT',
    'STL%': 'STL_PCT',
    'BLK%': 'BLK_PCT',
    'TOV%': 'TOV_PCT',
    'USG%': 'USG_PCT'
}

PLAYER_BOXSCORE_COLUMNS = [
    'FG', 'FGA', 'FG%', '3P', '3PA', '3P%', 'FT', 'FTA', 'FT%', 'ORB', 'DRB',
    'TRB', 'AST', 'STL', 'BLK', 'TOV', 'PF', 'PTS', 'GmSc', '+/-'
]
PLAYER_BOXSCORE_CONVERTERS = {col: float for col in PLAYER_BOXSCORE_COLUMNS}

def get_player_boxscore_url(player_id, year):
    return f'https://www.basketball-reference.com/players/{player_id[0]}/{player_id}/gamelog/{year}'

def get_player_boxscore(player_id, year):
    url = get_player_boxscore_url(player_id, year)
    tables = pd.read_html(url, attrs={'id': 'pgl_basic'}, parse_dates=True)
    if len(tables) != 1:
        raise ValueError
    df = tables[0]
    df = df[df['Rk'] != 'Rk']
    df = df.astype(PLAYER_BOXSCORE_CONVERTERS)
    return df

def get_games(year, month, day):
    resp = requests.get(GAMES_DAY_URL.format(
        year=year,
        month=month,
        day=day
    ))
    html = resp.text
    game_ids = re.findall(r'/boxscores/(.*?).html', html)[::4]
    tables = pd.read_html(html)[:len(game_ids)*3:3]
    team_names = [list(table[0].values) for table in tables]
    data = []
    for game_id, team_name in zip(game_ids, team_names):
        data.append({
            'away': team_name[0],
            'home': team_name[1],
            'game_id': game_id
        })
    return data

def get_stats(game_id, away, home):
    game_year = int(game_id[:4])
    game_month = int(game_id[4:6])
    game_day = int(game_id[6:8])
    tables = pd.read_html(BOX_SCORE_URL.format(game_id=game_id))
    num_tables = len(tables)
    away_basic_idx = 0
    away_advanced_idx = int(num_tables/2)-1
    home_basic_idx = int(num_tables/2)
    home_advanced_idx = num_tables-1
    tables = [
        tables[away_basic_idx],
        tables[away_advanced_idx],
        tables[home_basic_idx],
        tables[home_advanced_idx]
    ]
    for n, df in enumerate(tables):
        df.columns = df.columns.droplevel()
        df = df[df.MP.str.contains(':', na=False)].reset_index(drop=True)
        df = df.rename(columns=COLUMN_MAPPING)
        df['GAME'] = game_id
        df['YEAR'] = game_year
        df['MONTH'] = game_month
        df['DAY'] = game_day
        tables[n] = df
    tables[0]['TEAM'] = away
    tables[0]['HOME'] = 0
    tables[0]['AWAY'] = 1
    tables[1]['TEAM'] = away
    tables[1]['HOME'] = 0
    tables[1]['AWAY'] = 1
    tables[2]['TEAM'] = home
    tables[2]['HOME'] = 1
    tables[2]['AWAY'] = 0
    tables[3]['TEAM'] = home
    tables[3]['HOME'] = 1
    tables[3]['AWAY'] = 0
    basic_table = pd.concat([tables[0], tables[2]])
    advanced_table = pd.concat([tables[1], tables[3]])
    return {
        'basic': basic_table,
        'advanced': advanced_table
    }
